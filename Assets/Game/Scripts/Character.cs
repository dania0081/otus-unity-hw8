using System.Collections;
using TMPro;
using UnityEngine;

namespace Game.Scripts
{
    public class Character : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI HwText;
        [SerializeField]
        private Animator _animator;
        
        public int Health;

        public IEnumerator Attack(Character attackedCharacter)
        {
            Debug.Log($"{GetType().Name}.Attack: gameObject.name = {gameObject.name} => {attackedCharacter.gameObject.name}");

            _animator.SetTrigger("Shoot");

            yield return new WaitForSeconds(2f);
            
            attackedCharacter.Health -= 1;

            if (attackedCharacter.Health <= 0)
            {
                attackedCharacter.Die();
            }
        }

        private void Die()
        {
            Debug.Log($"{GetType().Name}.Die: name = {name}");
            StartCoroutine(HomeWorkCoroutine());
        }

        private IEnumerator HomeWorkCoroutine()
        {
            HwText.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            HwText.text = "4";
            yield return new WaitForSeconds(0.5f);
            HwText.text = "3";
            yield return new WaitForSeconds(0.5f);
            HwText.text = "2";
            yield return new WaitForSeconds(0.5f);
            HwText.text = "1";
            yield return new WaitForSeconds(0.5f);
            HwText.text = "Finish him!";
        }
    }
}